import os
import json

from dotenv import load_dotenv

load_dotenv()

key = os.getenv('key')

import eospyo


 

print("Claiming Asset")
data = {
    "recipient": "hyogasereiou",
    "max_count": 20,
    "nonce": 1,

}

auth = eospyo.Authorization(
    actor="alexkqatest1",
    permission="active",
)

action = eospyo.Action(
    account="open.facings",
    name="claim",
    data=data,
    authorization=[auth],
)

raw_transaction = eospyo.Transaction(actions=[action])
net = eospyo.WaxMainnet()
linked_transaction = raw_transaction.link(net=net)
signed_transaction = linked_transaction.sign(key=key)


resp = signed_transaction.send()

print("Printing the response")
resp_fmt = json.dumps(resp, indent=4)
print(f"Response:\n{resp_fmt}")


